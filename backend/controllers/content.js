import http from '../helpers/http-response'
import db from "../config/db"
import * as lang from "../helpers/handleFileLang"

export async function handleGetLang(req, res) {
  try {
    return res.status(200).send(http[200]("Content", {indo : lang.readLangFromJson('id') , eng : lang.readLangFromJson('en') }))
  } catch (err) {
    return res.status(500).send(http[500](err.message))
  }
}

export async function handleWriteLang(req, res){
  const b = req.body
  const isSameProcess = req.body.is_same_process
  const typeContent = req.body.type_content
  const typeLang = req.body.type_lang
  try {
    let langUpdate = {}
    if(isSameProcess == 'true'){
      let indoLangUpdate = lang.readLangFromJson('id')
      let englishLangUpdate = lang.readLangFromJson('en')
      Object.keys(b).map(item =>{
        indoLangUpdate[typeContent][item] = req.body[item]
        englishLangUpdate[typeContent][item] = req.body[item]
      })

      lang.writeLangToJson(indoLangUpdate, 'id')
      lang.writeLangToJson(englishLangUpdate, 'en')

      langUpdate = {indoLangUpdate, englishLangUpdate }
    }else {
      let updateLang = lang.readLangFromJson(typeLang)
      Object.keys(b).map(item =>{
        updateLang[typeContent][item] = req.body[item]
      })

      lang.writeLangToJson(updateLang, typeLang)
      langUpdate = {...updateLang}
    }

    return res.status(200).send(http[200](`Lang ${typeLang || 'id and en'} part ${typeContent} Update`, langUpdate))

  } catch (err) {
    return res.status(500).send(http[500](err.message))
  }
}
