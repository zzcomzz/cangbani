import http from '../helpers/http-response'
import db from "../config/db"
import jwt from 'jsonwebtoken'
import bycrypt from 'bcryptjs'

const JWT_SECRET_KEY = 'jwt-secret-key'

export async function getAllUser(req, res){
  try {

    const [allUser] = await db.query('SELECT username, email, created_at FROM users')

    res.status(200).send(http[200]('get all user', allUser))
  } catch (error) {
    res.status(500).send(http[500](error.message))
  }
}

export async function register(req, res){
  const email = req.body.email ? req.body.email.toLowerCase() : req.body.email
  const password = req.body.password
  const username = req.body.username
  try {

    if(password == undefined || email == undefined || email == '' || password == '' || username == ''){
      return res.status(400).send(http[400]('form field wajib diisi', null))
    }

    const [isUser] = await db.query('SELECT email FROM users WHERE email = ?', [email])

    if(isUser.length != 0){
      return res.status(400).send(http[400]('user sudah terdaftar', isUser))
    }

    const hassPass = await bycrypt.hash(password, 12)
    const [user] = await db.query('INSERT INTO users SET ?', {
      username : req.body.username,
      password : hassPass,
      email : email,
    })

    res.status(200).send(http[200]('Sukses Mendaftarkan User', user))

  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}

export async function login(req, res){
  const email = req.body.email ? req.body.email.toLowerCase() : req.body.email
  const password = req.body.password

  try {
    if(password == undefined || email == undefined || email == '' || password == ''){
      return res.status(400).send(http[400]('form field wajib diisi', null))
    }

    const [isUser]   = await db.query('SELECT * FROM users WHERE email = ?', [email])

    if(isUser.length == 0){
      return res.status(400).send(http[400]('user belum terdaftar', null))
    }
    const token = jwt.sign(email, JWT_SECRET_KEY)
    const userPass = isUser[0].password

    const isPassValid = await bycrypt.compare(password, userPass)

    if(isPassValid){
      return res.status(200).send(http[200]('login sukses', {
        token,
        user : isUser[0]
      }))
    }

    return res.status(400).send(http[400]('password salah', null))
  } catch (error) {
    res.status(500).send(http[500](error.message))
  }
}

export async function data(req, res) {
  const authToken = req.headers.authorization.split(' ')[1]
  res.locals.userdata = verifyToken(authToken)
  res.status(200).send(http[200]('OK', res.locals.userdata))
}

const verifyToken = token => jwt.verify(token, JWT_SECRET_KEY)
