import db from "../config/db"
import http from '../helpers/http-response'
import * as contentType from "../helpers/contentType"


export async function addPrivacyPolicy(req, res){
  try {
    res.status(200).send(http[200]("Kebijakan Privasi Berhasil diupload", null))
  } catch (error) {
    res.status(500).send(http[500](error.message))
  }
}

export async function deleteImage(req, res){
  try {

    const [image] = await db.query("DELETE FROM images WHERE image_id = ?", [ req.params.image_id])

    res.status(200).send(http[200]("image berhasil di hapus", null))
  } catch (error) {
    res.status(500).send(http[500](error.message))
  }
}

export async function hideImage(req, res){
  try {

    const [image] = await db.query("UPDATE images SET is_show = ? WHERE image_id = ?", [req.body.is_show, req.body.image_id])

    res.status(200).send(http[200]("image berhasil di update", {image}))
  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}

export async function getAllImages(req, res){
  try {

    const [images] = await db.query("SELECT * FROM images")

    res.status(200).send(http[200]("All Images", {
      images,
    }))
  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}

export async function getCountImages(req, res){
  try {

    const [images] = await db.query("SELECT * FROM images WHERE is_show = 1")

    const sertifikat = images.filter(img => img.content_type_id == contentType.CONTENT_IMAGE_SERTIFIKASI)
    const partner = images.filter(img => img.content_type_id == contentType.CONTENT_IMAGE_PARTNER)
    const works = images.filter(img => img.content_type_id == contentType.CONTENT_IMAGE_WORKS)
    res.status(200).send(http[200]("Sukses Get Images", {
      sertifikat : sertifikat.length,
      partner : partner.length,
      works : works.length,
      images,
    }))
  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}

export async function addSertifImage(req , res){
  try {

    await db.query('INSERT INTO images SET ?', {
      url_image : req.sertifFile,
      content_type_id : contentType.CONTENT_IMAGE_SERTIFIKASI
    })

    res.status(200).send(http[200]("image sertifikasi berhasil di upload", {image : req.sertifFile }))
  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}

export async function addPartnerImage(req , res){
  try {

    await db.query('INSERT INTO images SET ?', {
      url_image : req.partnerFile,
      content_type_id : contentType.CONTENT_IMAGE_PARTNER
    })

    res.status(200).send(http[200]("image partner & client berhasil di upload", {image : req.partnerFile }))
  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}

export async function addWorksImage(req , res){
  try {

    await db.query('INSERT INTO images SET ?', {
      url_image : req.worksFile,
      content_type_id : contentType.CONTENT_IMAGE_WORKS
    })

    res.status(200).send(http[200]("image pekerjaan kami berhasil di upload", {image : req.worksFile }))
  } catch (err) {
    res.status(500).send(http[500](err.message))
  }
}
