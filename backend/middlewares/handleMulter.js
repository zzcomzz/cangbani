import Multer from "multer"
import path from "path"

export const multer = Multer

export const uploadSertif = Multer.diskStorage({
  destination: (req, file, cb) => {
    if (
      file.mimetype !== 'image/png' &&
      file.mimetype !== 'image/jpg' &&
      file.mimetype !== 'image/jpeg'
    ) {
      cb(true, '');
    } else {
      cb(null, 'frontend/static/assets/sertifikasi/');
    }
  },
  filename: (req, file, cb) => {
    req.sertifFile = '/assets/sertifikasi/' + `${Date.now()}-${file.originalname}`
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

export const uploadPartner = Multer.diskStorage({
  destination: (req, file, cb) => {
    if (
      file.mimetype !== 'image/png' &&
      file.mimetype !== 'image/jpg' &&
      file.mimetype !== 'image/jpeg'
    ) {
      cb(true, '');
    } else {
      cb(null, 'frontend/static/assets/partner/');
    }
  },
  filename: (req, file, cb) => {
    req.partnerFile = '/assets/partner/' + `${Date.now()}-${file.originalname}`
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

export const uploadWorks = Multer.diskStorage({
  destination: (req, file, cb) => {
    if (
      file.mimetype !== 'image/png' &&
      file.mimetype !== 'image/jpg' &&
      file.mimetype !== 'image/jpeg'
    ) {
      cb(true, '');
    } else {
      cb(null, 'frontend/static/assets/works/');
    }
  },
  filename: (req, file, cb) => {
    req.worksFile = '/assets/works/' + `${Date.now()}-${file.originalname}`
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

export const uploadPrivacyPolicy = Multer.diskStorage({
  destination : (req, file, cb) =>{
    if (
      file.mimetype !== 'application/pdf'
    ) {
      cb(true, '');
    } else {
      cb(null, 'frontend/static/kebijakan-privasi/');
    }
  },
  filename: (req, file, cb) => {
    cb(null, 'Privacy_Policy'+path.extname(file.originalname));
  },
})
