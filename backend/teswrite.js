const data = JSON.stringify(
  {
    "links": {
      "home": "Home",
      "about": "About",
      "english": "English version",
      "french": "French version"
    },
    "home": {
      "title": "Selamat Datang saudara",
      "introduction": "This is an introduction in English."
    },
    "about": {
      "title": "About",
      "introduction": "This page is made to give you more informations."
    }
  },
  null,
  2
)

const fs = require('fs')
const path = require('path')

// fs.writeFileSync(path.resolve('frontend/lang/id.json'),data, {encoding : 'utf8'})

const file = fs.readFileSync(path.resolve('frontend/lang/id.json'),{encoding : 'utf8'})
