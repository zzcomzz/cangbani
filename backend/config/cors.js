import cors from 'cors'

const allowedOrigins = [
  `http://localhost:${process.env.PORT}`,
  `http://${process.env.HOST}:${process.env.PORT}`,
  `https://${process.env.HOST}`
]

const delegate = (origin, callback) => {
  if (!origin) return callback(null, true)

  if (allowedOrigins.indexOf(origin) === -1) {
    console.info(origin)
    return callback(
      new Error('Sorry, This Origin not allowed for this route.'),
      false
    )
  }

  return callback(null, true)
}

export default (opt = {}) => cors({ origin: delegate, ...opt })
