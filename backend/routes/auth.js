import { Router } from "express"
import * as Auth from "../controllers/user"

const router = Router()

router.get('/get-users', Auth.getAllUser)
router.post('/register', Auth.register)
router.post('/login', Auth.login)
router.post('/me', Auth.data)

export default router
