import { Router } from "express"
import * as controllersImage from "../controllers/images"
import * as Multer from "../middlewares/handleMulter"

const router = Router()

router.post('/add-sertifikasi',
  Multer.multer({storage : Multer.uploadSertif}).single('sertifikasi'),
  controllersImage.addSertifImage
)

router.post('/add-partner',
  Multer.multer({storage : Multer.uploadPartner}).single('partner'),
  controllersImage.addPartnerImage
)

router.post('/add-works',
  Multer.multer({storage : Multer.uploadWorks}).single('works'),
  controllersImage.addWorksImage
)

router.get('/get-images-dashboard', controllersImage.getAllImages)
router.get('/get-images', controllersImage.getCountImages)
router.put('/update-image', controllersImage.hideImage)
router.delete('/delete-image/:image_id', controllersImage.deleteImage)
export default router
