import { Router } from "express"
import auth from "./auth"
import images from "./images"
import content from "./content"

const router = Router()

router.use('/auth', auth)
router.use('/images', images)
router.use('/content', content)

export default router
