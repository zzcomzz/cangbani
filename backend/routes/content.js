import { Router } from "express"
const router = Router()

import * as content from "../controllers/content"
import * as controllersImage from "../controllers/images"
import * as Multer from "../middlewares/handleMulter"

router.post('/add-pdf',
  Multer.multer({storage : Multer.uploadPrivacyPolicy}).single('kebijakan'),
  controllersImage.addPrivacyPolicy
)

router.get('/get-lang', content.handleGetLang)
router.put('/edit-lang', content.handleWriteLang)


export default router
