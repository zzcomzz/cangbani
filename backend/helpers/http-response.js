export default {
  200: (msg, data) => ({
    code: 200,
    status: 'OK',
    success : true,
    message: msg,
    data
  }),
  400: (msg, data) => ({
    code: 400,
    status: 'Bad Request',
    success : false,
    message: msg,
    data
  }),
  401: (msg, data) => ({
    code: 401,
    status: 'Unauthorized',
    success : false,
    message: msg,
    data
  }),
  403: (msg, data) => ({
    code: 403,
    status: 'Forbidden',
    success : false,
    message: msg,
    data
  }),
  404: (msg, data) => ({
    code: 404,
    status: 'Not Found',
    success : false,
    message: msg,
    data
  }),
  405: (msg, data) => ({
    code: 405,
    status: 'Method Not Allowed',
    success : false,
    message: msg,
    data
  }),
  500: (msg, data) => ({
    code: 500,
    status: 'Internal Server Error',
    success : false,
    message: msg,
    data
  }),
  exception: (code, msg) => Object({ code: code, message: msg })
}
