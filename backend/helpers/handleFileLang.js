import db from "../config/db"
import fs from "fs"
import path from "path"


export const writeLangToJson = (dataLang, isLang = '') => {
  fs.writeFileSync(path.resolve('frontend/lang/'+ isLang +'.json'), dataWriteFormat(dataLang), {encoding : 'utf8'})
}

export const readLangFromJson = (isLang = '') => {
  return JSON.parse(fs.readFileSync(path.resolve('frontend/lang/' + isLang +'.json'),{encoding : 'utf8'}))
}

const dataWriteFormat = (dataJson) => JSON.stringify(dataJson, null, 2)
