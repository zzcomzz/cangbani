module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  // add your custom rules here
  rules: {
    'curly': 'off',
    'no-console': 'off',
    'no-return-assign': 'off',
    'arrow-parens': 'off',
    'no-unreachable': 'off',
    'no-useless-return': 'off',
    'no-return-await': 'off',
    'no-new': 'off',
    'nuxt/no-cjs-in-config': 'off'
  }
}
