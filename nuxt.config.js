require('dotenv').config({path : '.env'})
import webpack from 'webpack'

const baseurl = process.env.NODE_ENV === 'production'
  ? `https://${process.env.HOST}/`
  : `http://${process.env.HOST}:${process.env.PORT}/`


let axiosOpt = {}

if (process.env.NODE_ENV === 'production') {
  axiosOpt = {
    baseURL: baseurl,
    https: true
  }
}

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  // mode: 'spa',
  ssr : false,
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  srcDir : 'frontend/',


  env : {
    BASEURL : baseurl
  },
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    titleTemplate: '%s - ' + 'PT. Cang Bani Industries',
    title: 'Cang Bani' || '',
    metaTemplate : 'Cang Bani',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'  },
      { hid: 'description', name: 'description', content: 'PT. Cang Bani industries - Tugas Kami Membangun Dan Membuat' },
      { hid: 'og:description', name: 'og:description', content: 'PT. Cang Bani industries - Tugas Kami Membangun Dan Membuat' },
      { hid: 'title', name: 'title', content: 'Cang Bani' },
      { hid: 'og:title', name: 'og:title', content: 'Cang Bani' }
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: "stylesheet",
        href: "/assets/fonts.css"
      },
      // {
      //   rel: "stylesheet",
      //   href: "/assets/owl/dist/assets/owl.theme.default.min.css"
      // },
      // {
      //   rel: "stylesheet",
      //   type: "text/css",
      //   href: "https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css"
      // },
      // {
      //   rel: "stylesheet",
      //   // href: "/assets/fa/css/font-awesome.min.css"
      //   href: "/vendor/fontawesome-free/css/all.min.css"
      // },
      // {
      //   rel: "stylesheet",
      //   href: "/css/sb-admin-2.min.css"
      // },
    ],

    script : [
      {
        src: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
        // src : "/vendor/bootstrap/js/bootstrap.bundle.min.js",
        type: "text/javascript"
      },
      {
        src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js",
        type: "text/javascript"
      },
      {
        // src: "https://code.jquery.com/jquery-3.4.1.slim.min.js",
        // src: "/vendor/jquery/jquery.min.js",
        src: "/assets/jquery.min.js",
        type: "text/javascript",
      },
      // {
      //   type: "text/javascript",
      //   src: "/assets/owl/dist/owl.carousel.min.js",
      //   defer : true
      // },
      // {
      //   type: "text/javascript",
      //   src: "/assets/owl/dist/owl.carousel.js",
      //   defer : true
      // },
    ],
  },

  loading: '~/components/Loading.vue',
  /*
  ** Global CSS
  */
  css: [
    '~/assets/fonts.css'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins : [
    '~/plugins/i18n.js',
    { src : '~/plugins/jquery.min.js', ssr : false },
    { src: '~/plugins/after-each.js', ssr : false },
    { src : '~/plugins/owl.carousel.min.js', ssr : false }
  ],

  router : {
  },
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/dotenv',
    'vue-sweetalert2/nuxt',

    ['nuxt-i18n', {
      baseUrl : baseurl,
      seo: true,
      locales: [
        {
          name: 'English',
          code: 'en',
          iso: 'en-US',
          file: 'en.js'
        },
        {
          name: 'Bahasa Indonesia',
          code: 'id',
          iso: 'id-ID',
          file: 'id.js'
        },
      ],
      strategy : 'prefix_except_default',
      langDir: 'lang/',
      lazy : true,
      defaultLocale: 'id',
      loadLanguagesAsync: true,
      parsePages: false,
      pages : {
        '_lang/about-us' : {
          id : '/tentang-kami',
          en : '/about-us'
        },
        '_lang/contact-us' : {
          id : '/hubungi-kami',
          en : '/contact-us'
        },
        '_lang/privacy-policy' : {
          id : '/kebijakan-privasi',
          en : '/privacy-policy'
        },
        '_lang/our-works' : {
          id : '/pekerjaan-kami',
          en : '/our-works'
        },
        'pekerjaan-kami' : {
          id : '/pekerjaan-kami',
          en : '/our-works'
        },
        'kebijakan-privasi' : {
          id : '/kebijakan-privasi',
          en : '/privacy-policy'
        },
        'tentang-kami' : {
          id : '/tentang-kami',
          en : '/about-us'
        },
        'hubungi-kami' : {
          id : '/hubungi-kami',
          en : '/contact-us'
        },
        '_lang/' : {
          id : '/',
          en : '/en'
        },
      }
    }],
  ],

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */

  axios : axiosOpt,

  auth : {
    cookie: false,
    strategies: {
      local: {
        endpoints: {
          login: { url: 'api/auth/login', method: 'post', propertyName: 'data.token' },
          logout: { url: 'api/auth/logout', method: 'post' },
          user: { url: 'api/auth/me', method: 'post', propertyName: 'data' }
        }
      },
      tokenType: false
    },
    redirect: {
      login: '/admin/login',
      logout: '/admin/login',
      callback: '/admin/login',
      home: '/admin/dashboard'
    }
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  // vuetify: {
  //   customVariables: ['~/assets/variables.scss'],
  //   theme: {
  //     dark: true,
  //     themes: {
  //       dark: {
  //         primary: colors.blue.darken2,
  //         accent: colors.grey.darken3,
  //         secondary: colors.amber.darken3,
  //         info: colors.teal.lighten1,
  //         warning: colors.amber.base,
  //         error: colors.deepOrange.accent4,
  //         success: colors.green.accent3
  //       }
  //     }
  //   }
  // },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    plugins : [
      new webpack.ProvidePlugin({
        $ : 'jquery',
        jQuery : 'jquery',
        'window.jQuery' : 'jquery'
      })
    ]
  },
  telemetry: false
}
