export default ({app, store}) => {
  app.i18n.path = (link) => {

    if(app.i18n.locale === app.i18n.defaultLocale){
      return `/${link}`
    }
    return `/${app.i18n.locale}/${link}`
  }
}
