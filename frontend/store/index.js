export const state = () => ({
  langs : ['id', 'en'],
  lang : localStorage.getItem('lang') || 'id',
  indoLang : [],
  englishLang : [],
  imagesDashboards : [],
  imagesHome : []
})


export const mutations = {
  SET_LANG(state, lang) {
    if(state.langs.includes(lang)){
      localStorage.setItem('lang', lang)
      state.lang = lang
    }
  },
  SET_INDO_LANG(state, indoLang){
    state.indoLang = indoLang
  },
  SET_ENGLISH_LANG(state, englishLang){
    state.englishLang = englishLang
  },
  getImageDashboards(state, images){
    state.imagesDashboards = images
  },
  getImagesHomePage(state, images){
    state.imagesHome = images
  }
}

export const actions = {
  async getLangContent({commit}){
    const langs = await this.$axios.$get('/api/content/get-lang').catch(err => this.$swal('Oops', err.response.data.message, 'error'))
    if(langs.success){

      commit('SET_INDO_LANG', langs.data.indo)
      commit('SET_ENGLISH_LANG', langs.data.eng)
    }
  },
  showLoadingPage(ctx, condition){

    if(condition){
      this.$swal({
        showConfirmButton : false,
        title : '',
        html : '<i style="font-size:50px;" class="fa fa-spinner fa-spin"></i>',
        allowOutsideClick : false,
        footer: '<h6>Loading...</h6>',
      })
    }else {
      this.$swal().close()
    }
  },
  async GET_IMAGES_DASHBOARD({commit }){
    const images = await this.$axios.$get('api/images/get-images-dashboard').catch(err => this.$swal('Oops', err.response.data.message, 'error'))
    if(images.success){
      commit('getImageDashboards', images.data.images)
    }
  },
  async GET_IMAGES_HOMEPAGE({commit}){
    const images = await this.$axios.$get('/api/images/get-images').catch(err => this.$swal('Maaf', err.response.data.message, 'error'))
    if(images.success){
      commit('getImagesHomePage', images.data.images)
    }
  }
}
