const util = require("util")
const cron = require("node-cron")

const exec = util.promisify(require("child_process").exec)

async function runScript(){
  const { stdout , stderr}   = await exec("php cron.php")
  console.log("STD OUT : \n", stdout)
  console.log("STD ERR : \n", stderr)
  if(stderr){
    await runScript()
  }
}

runScript()

// cron.schedule("0 1 * * *", () => {
//   runScript()
// }, {
//   scheduled : true,
//   timezone : "Asia/Bangkok"
// })
