import http from 'http'
import bodyParser from 'body-parser'
import cors from './backend/config/cors'
import express from 'express'
import nuxtConf from './nuxt.config'
import db from "./backend/config/db"
import {Nuxt, Builder} from 'nuxt'

import routes from './backend/routes'

global.__basedir = __dirname;


const app = express()

const nuxt = new Nuxt(nuxtConf)

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(cors())
app.use("/api",routes)
app.use(nuxt.render)
app.disable('x-powered-by');

const PORT = process.env.PORT || 3000;

const server = http.createServer(app)

async function serverStart(){
  try {
    // await db.connect()
    await db.getConnection()
    if(process.env.NODE_ENV == "production") {
      await nuxt.ready()
    }else {
      const builder = new Builder(nuxt)
      await builder.build()
    }

    server.listen(PORT, () => console.log(`Server Listen on http://localhost:${PORT}`))
  } catch (error) {
    // serverStart()
    console.log(error);
  }
}

serverStart()
